import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TimeUtilsTest {
   private int inputNumber;
   private String expectedResult;
   private TimeUtils timeutils;

   @Before
   public void initialize() {
      timeutils = new TimeUtils();
   }

   // Each parameter should be placed as an argument here
   // Every time runner triggers, it will pass the arguments
   // from parameters we defined in primeNumbers() method
	
   public TimeUtilsTest(int inputNumber, String expectedResult) {
      this.inputNumber = inputNumber;
      this.expectedResult = expectedResult;
   }

   @Parameterized.Parameters
   public static Collection TimeUtils() {
      return Arrays.asList(new Object[][] {
         { 2, "0:00:02" },
         { 61, "0:01:01" },
         { 3600, "1:00:00" },
         { 28800, "8:00:00" },
         { -100, "-1" },
         { 100000, "-1" }
      });
   }

   // This test will run 4 times since we have 5 parameters defined
   @Test
   public void testTimeUtilsTest() {
      System.out.println("Parameterized Number is : " + inputNumber);
      assertEquals(expectedResult, 
      timeutils.secToTime(inputNumber));
   }
}